﻿/*
 * Array (pole) je kolekce hodnot v jedné proměnné. 
 * Abychom nemuseli každou proměnou deklarovat zvlášť.
 * Pole označujeme hranatými závorkami.
 * 
 * Příklad: 
 *   Ve třídě je 30 studentů a každému chceme dát známku.
 *   Nejprve vytvořím pole o velikosti 30.
 */

int[] marks = new int[30];

/*
 * Pole jsou předávány odkazem, takže musím použít "new".
 * Nyní mám proměnnou, která v sobě může mít až 30 různých hodnot.
 * Dám známku prvnímu studentovi.
 */

marks[0] = 3;

/*
 * Každá hodnota v poli má svůj "index".
 * Index začíná od 0 (takže pole o velikosti 30 bude mít indexy [0..29])
 * K jednotlivým hodnotám přistupuji přes index v hranatých závorkách.
 * 
 *  
 * 01 - Dejte známku druhému studentovi. (netřeba kopírovat, zkuste začít psát "mar" a uvidíte, jak VS napovídá. Tab + Tab)
 */



/*
 *  Také nezapomeňte, že proměnnou z cyklu "for" můžeme používat.
 *  Rozdám známky dalším 8mi studentům.
 */

for (int i = 2; i < 10; i++)
{
    marks[i] = 2;
}

/*
 *  02 - Rozdejte známky všem zbývajícím studentům.
 *  Známky dejte náhodné v rozsahu [1,5]
 */


/*
 * 03 - Nyní s pomocí cyklu vypište na jeden řádek všechny známky
 *   3, X, 2, 2, 2,...
 */


/*
 * 04 - S pomocí cyklu projděte všechny známky a spočítejte, kolik bylo rozdáno 5tek.
 * Výsledek vypište na obrazovku.
 */


/*
 * 05 - S pomocí cyklu projděte všechny známky a spočítejte jednotlivé známky.
 * Výsledek vypište na obrazovku. (kolik bylo 1ček, kolik 2jek,...)
 */


/*
 * 06 - Vypište na obrazovku průměr třídy zaokrouhlený na 2 desetinná místa.
 */

/*
 * 07 - Vyměňte v poli první a druhou známku.
 */

/*
 * 08 - Projděte si alespoň jeden odkaz:
 *   https://dotnettutorials.net/lesson/arrays-csharp/
 *   https://www.tutorialspoint.com/cprogramming/c_arrays.htm
 *   https://www.programiz.com/csharp-programming/arrays
 */