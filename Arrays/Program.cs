﻿using _01_Arrays;

// Structures
CoordsStruct firstPointStruct;
firstPointStruct.X = 10;
firstPointStruct.Y = 20;

CoordsStruct secondPointStruct;
secondPointStruct = firstPointStruct;

firstPointStruct.X = 999;
Console.WriteLine(firstPointStruct.X);
Console.WriteLine(secondPointStruct.X);


// Records
CoordsRecord firstPointRecord;
firstPointRecord = new CoordsRecord() 
{ 
    X = 10, 
    Y = 20 
};

CoordsRecord secondPointRecord;
secondPointRecord = firstPointRecord;

firstPointRecord.X = 999;
Console.WriteLine(firstPointRecord.X);
Console.WriteLine(secondPointRecord.X);

/*
 * Vytvořte novou proměnnou typu CoordsStruct a nastavte jí hodnoty.
 * Vypište souřadnice na obrazovku.
 */

/*
 * Vytvořte novou proměnnou typu CoordsRecord a nastavte jí hodnoty (nezapomeňte na "new").
 * Vypište souřadnice na obrazovku.
 */

