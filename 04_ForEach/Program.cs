﻿
using _04_ForEach;

var alerts = new string[] { "Low Pressure", "High oil temperature", "Critical battery" };

/*
 * Klasický cyklus "for" už známe
 */

for (int i = 0; i < alerts.Length; i++)
{
    Console.WriteLine(alerts[i]);
}

/*
 * Ukažme si cyklus "foreach", který se používá často v kombinaci s arrays.
 * Dosáhneme stejného výsledku jako s "for"
 */

foreach (string alert in alerts)
{
    Console.WriteLine(alert);
}

/*
 * Který se Vám líbí více?
 * Cyklus "foreach" projde celé pole od začátku a každý prvek uloží do proměnné.
 * S touto proměnnou pak uvnitř cyklu můžeme libovolně pracovat.
 * Odpadá tím práce s indexy a hranatámi závorkami.
 * Jméno proměnné je na Vás, ale doporučuji se držet angličtiny 
 *   Pokud je pole "marks", pak proměnná ideálně "mark".
 *   "votes" -> "vote"
 *   "dates" -> "date"
 *   atd... 
 *   Angličtina se k tomu přímo nabízí
 */

/*
 * Prostudujte si následující implementaci CoinMachine a poté změnte cyklus na "foreach"
 */

var coinValues = new int[] { 50, 20, 10, 5, 2, 1 };
int amount = 389;

for (int i = 0; i < coinValues.Length; i++)
{
    amount = CoinHelpers.DispenseCoinsFrom(amount, coinValues[i]);
}

/*
 * Vzpomínáte ještě na variables? (https://gitlab.com/school9961818/variables/)
 * Algoritmus se nijak nezměnil: 
 *   Odečítám od vstupní hodnoty postupně ty největší mince a postupně vypisuji
 *   
 * Postupně jsme si ale představili další programátorské nástroje a konstrukty, 
 * které nám kód (pokud použity správně) zpřehledňují a zkracují.
 *   - Cykly
 *   - Metody
 *   - Arrays
 */

/*
 * Outro: https://gitlab.com/school9961818/arrays/-/blob/master/Outro.md
 */