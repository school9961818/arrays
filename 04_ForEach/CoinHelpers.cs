﻿namespace _04_ForEach
{
    public static class CoinHelpers
    {
        public static int DispenseCoinsFrom(int amount, int coinValue)
        {
            int coins = amount / coinValue;
            
            if (coins > 0)
            {
                Console.WriteLine($"Dispensing {coins}x{coinValue}");
            }

            return amount % coinValue;
        }

    }
}
