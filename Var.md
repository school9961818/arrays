Různé datové typy zabírají v paměti různě místa. Nelze je míchat dohromady. Visual studio to dokonce dokáže odhalit a zakázat ještě před spuštěním programu:

![Convert](Images/Convert.jpg)

Díky tomuto některé jazyky deklaraci datových typů ani neřeší. Například [JavaScript](https://www.tutorialsteacher.com/javascript/javascript-variable).

Také C# přidal možnost použít `var` (zkratka pro variable), která datový typ přiřadí až s použitou hodnotou. Tzv. dynamické typování.

``` csharp
int numberOne = 6;
var numberTwo = 7;

string textOne = "Arigato";
var textTwo = "Domo";
```

Každý přístup má svá pro a proti. Jedním z velkých negativ dynamického typování (`var`) je horší přehlednost kódu. Představme si program, ve kterém je chyba a podezíráme proměnnou `mysteryVariable`:

```csharp
var mysteryVariable; 

// some code...

mysteryVariable = GenericHelpers.VaguelyDefinedMethod(shadyParameterOne, shadyParameterTwo);

// some more code...

while (mysteryVariable != InsufficentlyNamedConstants.DesiredValue)
{
  // more code inside a cycle...

  if (ThirdPartyHelper.IsReallyComplicatedConditionMet())
  {
     mysteryVariable = InternetClientHelpers.CallAncientServiceWithoutDocumentationThatNobodyWantsToMaintain(weirdYetMandarotyParameter);
  }
}
```

Dokážete z kódu zjistit datový typ? Odhadnout hodnotu? Samozřejmě můžete správně argumentovat, že stačí kód psát přehledně. (JavaScript programátoři Vám jistě dají za pravdu). Už to ale není otázka, co je nebo není správné, ale jaký přístup komu vyhovuje. Hezky a poměrně nadčasově se tomu věnuje kniha [Clean code](https://books.google.cz/books/about/Clean_Code.html?id=dwSfGQAACAAJ&redir_esc=y)

### Nové pravidlo kurzu

`var` mohu použít jen v případě, že na stejném řádku vidím datový typ.

``` csharp

string[] inviteList = new string[] { "James", "Lucia" }; // všimněte si, že datový typ "string[]" je zde použit dvakrát

var banList = new string[] { "Robert", "Karen" }; // ačkoliv jsme použili "var", je na první pohled vidět, že proměnná je array stringů

```

Ještě jednou:
> `var` mohu použít jen v případě, že na stejném řádku vidím datový typ.

``` csharp

var marks = new int[] { 1, 2, 3, 4, 5 }; // OK

var point = new CoordinatesIn2D()
{
    X = 30,
    Y = 30
}; // also OK

var realNumber = 2.3; // not OK! Is it double? float? decimal? Nobody wants to remember that

var result = MathHelpers.GetMethodResult(); // not OK! Nobody wants to be a method detective for no reason

var tmpValue; // #!@#%$%!!!
```

### Pravidla kurzu (vyžadována při hodnocení)

* Nové datové typy definujeme ve vlastním souboru
* Žádná metoda není uvnitř Program.cs
* `var` mohu použít jen s datovým typem na stejném řádku
