﻿/*
 * Array můžeme vytvořit v jednom příkazu i s hodnotami.
 *   Např. pole se známkami v norském školství.
 */

var norwegianMarks = new int[] { 6, 5, 4, 3, 2, 1 };

/*
 * Všimněte si nového slova "var", které je použito místo datového typu.
 * Pojďme si ho přiblížit:
 *   https://gitlab.com/school9961818/arrays/-/blob/master/Var.md
 */

/*
 * 01 - Vytvořte nové pole s Dánskými známkami
 */


/*
 * Pole je "homogenní" datový typ, což znamená, že uvnitř může být jen jeden druh dat. 
 * Nemůžeme v poli míchat stringy a čísla.
 * Jinak ale můžeme vytvořit pole čehokoliv.
 */

var votesByParliamentMembers = new bool[] { true, true, false, false, false };

var mcDonaldShifts = new TimeSpan[] { TimeSpan.FromHours(4), TimeSpan.FromHours(6), TimeSpan.FromHours(8) };

/*
 * 02 - Vytvořte nové pole typu "DateTime[]", které bude obsahovat české státní svátky pro aktuální rok.
 */

/* 
 * 03 - Vypište na obrazovku, kolik svátků máme celkem (vygooglete "array length example")
 */

/*
 * 04 - Vypište na obrazovku, který je Váš nejméně oblíbený školní den a kolik těchto dní odpadne díky letošním svátkům.
 */

/*
 * 05 - Vyvořte metodu (pamatujte na pravidla kurzu)
 * Vstupní parametry
 *   DateTime
 * Výstupní parametry
 *   true/false podle toho, jestli datum vychází na víkend
 */

/*
 * 06 - S pomocí cyklu a předchozí metody vypište na obrazovku, kolik letošních svátků vychází na víkend a kolik na týden.
 */

/* 
 * 07 - Vypište na obrazovku, kolik dní zbývá do Vánoc.
 */

/* 
 * 08 - Vypište na obrazovku, kolik dní zbývá do Vánoc od prvního letošního svátku.
 */

/* 
 * 09 - Vypište na obrazovku, jestli letos máme nějaké svátky (vygooglete "array any example")
 */

/*
 * 10 - Vyberte nějaké datum (třeba Vaše narozeniny) a přidejte ho do pole státních svátků.
 * Neupravujte původní inicializaci. Přidejte nový prvek pomocí "append".
 */

/* 
 * 11 - Znovu vypište na obrazovku, kolik svátků máme celkem. Ověřte spuštěním, že "append" zafungoval.
 */






